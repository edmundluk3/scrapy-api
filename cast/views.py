# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import render

from .models import Cast
from .serializers import CastSerializer

# Create your views here.
class List(APIView):
    """
    list all cast
    """
    def get(self, request, format=None):
        cast = Cast.objects.all()
        serializer = CastSerializer(cast, many=True)
        return Response(serializer.data)

class Detail(APIView):
    def get(self, request, pk, format=None):
        return Response({'hello': 'cast detail %s' % (pk)})
