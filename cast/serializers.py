from rest_framework import serializers
from .models import Cast, TvCast

class CastSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cast
        fields = ('full_name', 'id')

class TvCastSerializer(serializers.ModelSerializer):
    class Meta:
        model = TvCast
        fields = ('tv_id', 'cast_id', 'id')
