from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    url(r'^$', views.List.as_view(), name='cast-list'),
    url(r'^(?P<pk>[0-9]+)/$', views.Detail.as_view(), name='cast-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)