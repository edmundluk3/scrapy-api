# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Cast(models.Model):
    full_name = models.CharField(max_length=50, unique=True)

# many to many relationshiop
class TvCast(models.Model):
    id = models.AutoField(primary_key=True)
    tv_id = models.IntegerField()
    cast_id = models.IntegerField()

# Create your models here.
