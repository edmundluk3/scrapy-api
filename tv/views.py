from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework.pagination import LimitOffsetPagination

from django.http import Http404
from django.conf import settings
from .serializers import TvSerializer, ThunderUrlSerializer
from .models import TV, Season, ThunderUrl
from cast.models import Cast, TvCast
from cast.serializers import CastSerializer, TvCastSerializer

# Create your views here.
class List(APIView):
    """
    list all tv
    """

    def get(self, request, format=None):
        pagination_class = LimitOffsetPagination
        paginator = pagination_class()

        tvs_queryset = TV.objects.all()

        tvs = paginator.paginate_queryset(tvs_queryset, request)

        serializer = TvSerializer(tvs, many=True)

        return paginator.get_paginated_response(serializer.data)
        #return Response(serializer.data)

    def post(self, request, format=None):
        tv_serializer = TvSerializer(data=request.data)

        if tv_serializer.is_valid():
            tv_serializer.save()

            thunders = request.data['thunders']
            cast = request.data['cast']

            for actor in cast:
                # fixme
                # name is unique
                actor, created = Cast.objects.get_or_create(full_name=actor)

                cast_serializer = CastSerializer(actor)

                data = {
                    'tv_id': tv_serializer.data['id'],
                    'cast_id': cast_serializer.data['id']
                }

                tv_cast_serializer = TvCastSerializer(data=data)

                if tv_cast_serializer.is_valid():
                    tv_cast_serializer.save()

            # save thunder urls
            for t in thunders:
                t['tv_id'] = tv_serializer.data['id']

            thunder_serializer = ThunderUrlSerializer(data=thunders, many=True)
            if thunder_serializer.is_valid():
                thunder_serializer.save()

                # TODO
                # add more data to response
                return Response(thunder_serializer.data, status=status.HTTP_201_CREATED)

            return Response(thunder_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(tv_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Detail(APIView):
    """
    Retrieve, update or delete a tv
    """
    def get_object(self, pk):
        try:
            return TV.objects.get(pk=pk)
        except TV.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        tv = self.get_object(pk)
        thunders = ThunderUrl.objects.filter(tv_id=pk)

        tv_serializer = TvSerializer(tv)
        thunder_serializer = ThunderUrlSerializer(thunders, many=True)

        res = tv_serializer.data
        res['thunders'] = thunder_serializer.data
        return Response(res)

    def put(self, request, pk, format=None):
        tv = self.get_object(pk)
        serializer = TvSerializer(tv, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        pass
        # tv = self.get_object(pk)
        # tv.delete()
        # return Response(status=status.HTTP_204_NO_CONTENT)
