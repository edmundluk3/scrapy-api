from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from .models import TV, ThunderUrl

class ThunderUrlSerializer(serializers.ModelSerializer):
    class Meta:
        model = ThunderUrl
        fields = ('name', 'url', 'tv_id')

class TvSerializer(serializers.ModelSerializer):
    class Meta:
        model = TV
        fields = (
            'title',
            'year',
            'id',
            'tv_type',
            'audio',
            'update_status',
            'update_date',
            'plot'
        )

        """
        title and year field combines a Unique
        """
        validators = [
            UniqueTogetherValidator(
                queryset=TV.objects.all(),
                fields=('title', 'year')
            )
        ]
