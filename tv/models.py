from django.db import models

# Create your models here.
class TV(models.Model):
    title = models.CharField(max_length=500)

    # publish year
    year = models.CharField(max_length=50)

    # drama or movie publish area
    location = models.CharField(max_length=500)

    # lastest episode
    update_status = models.CharField(max_length=500)

    # category of the drama or movie
    tv_type = models.CharField(max_length=500)

    # audio channel
    audio = models.CharField(max_length=500)

    # scrapy source
    source = models.CharField(max_length=500)

    # fixme
    update_date = models.CharField(max_length=500)

    # create date in system
    create_date = models.DateTimeField(auto_now=True)

    # short description
    plot = models.TextField()

    #def as_dict(self):
    #    return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Season(models.Model):
    pass

class ThunderUrl(models.Model):
    name = models.CharField(max_length=500)
    url = models.CharField(max_length=1000)

    # many to one relationshiop
    tv_id = models.IntegerField()
