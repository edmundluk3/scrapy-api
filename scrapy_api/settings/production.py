from .common import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'scrapy_api',
        'USER': 'postgres',
        'PASSWORD': 'poiulkj',
        'HOST': '127.0.0.1',
        'PORT': '5432'
    }
}

STATIC_ROOT = os.path.join(BASE_DIR, 'static/')
